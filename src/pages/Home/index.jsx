import { useEffect, useState } from 'react'
import { connect, useDispatch } from 'react-redux'
import { MdAddShoppingCart } from 'react-icons/md'

import api from '../../services/api'
import { Container } from '../../components/Container'
import { formatPrice } from '../../util/format'
import * as cartActions from '../../store/modules/cart/actions'

import * as S from './styles'

const Home = ({ amount }) => {
  const [products, setProducts] = useState([])
  const dispatch = useDispatch()

  useEffect(() => {
    const fetchData = async () => {
      const response = await api.get('/products')

      const data = response.data.map((product) => ({
        ...product,
        priceFormatted: formatPrice(product.price)
      }))

      setProducts(data)
    }

    fetchData()
  }, [])

  const handleAddProductToCart = (product) => {
    dispatch(cartActions.addToCart(product))
  }

  return (
    <Container>
      <S.Wrapper>
        {products.map((product) => (
          <li key={product.id}>
            <img src={product.image} alt="" />
            <strong>{product.title}</strong>
            <span>{product.priceFormatted}</span>

            <button
              type="button"
              onClick={() => handleAddProductToCart(product)}
            >
              <div>
                <MdAddShoppingCart size={16} color="#fff" />
                {amount[product.id] || 0}
              </div>
              <span>Adicionar ao carrinho</span>
            </button>
          </li>
        ))}
      </S.Wrapper>
    </Container>
  )
}

const mapStateToProps = (state) => ({
  amount: state.cart.reduce((amount, product) => {
    amount[product.id] = product.amount
    return amount
  }, {})
})

export default connect(mapStateToProps)(Home)
