import {
  MdRemoveCircleOutline,
  MdAddCircleOutline,
  MdDelete
} from 'react-icons/md'
import { connect } from 'react-redux'

import * as cartActions from '../../store/modules/cart/actions'
import { Container } from '../../components/Container'
import * as S from './styles'
import { bindActionCreators } from 'redux'
import { formatPrice } from '../../util/format'

const Cart = ({ cart, removeFromCart, updateAmount, total }) => {
  const increment = (product) => {
    updateAmount(product.id, product.amount + 1)
  }

  const decrement = (product) => {
    updateAmount(product.id, product.amount - 1)
  }

  return (
    <Container>
      <S.Wrapper>
        <S.ProductTable>
          <thead>
            <tr>
              <th />
              <th>Produto</th>
              <th>Qtd</th>
              <th>Subtotal</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {cart.map((product) => (
              <tr key={product.id}>
                <td>
                  <img src={product.image} alt="Tênis" />
                </td>
                <td>
                  <strong>{product.title}</strong>
                  <span>{product.priceFormatted}</span>
                </td>
                <td>
                  <S.QuantityBtn>
                    <button type="button" onClick={() => decrement(product)}>
                      <MdRemoveCircleOutline size={20} color="#7159c1" />
                    </button>
                    <input type="number" readOnly value={product.amount} />
                    <button type="button" onClick={() => increment(product)}>
                      <MdAddCircleOutline size={20} color="#7159c1" />
                    </button>
                  </S.QuantityBtn>
                </td>
                <td>
                  <strong>{product.subTotal}</strong>
                </td>
                <td>
                  <button
                    type="button"
                    onClick={() => removeFromCart(product.id)}
                  >
                    <MdDelete size={20} color="#7159c1" />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </S.ProductTable>

        <S.Footer>
          <button type="button">Finalizar pedido</button>

          <div>
            <span>Total</span>
            <strong>{total}</strong>
          </div>
        </S.Footer>
      </S.Wrapper>
    </Container>
  )
}

const mapStateToProps = (state) => ({
  cart: state.cart.map((product) => ({
    ...product,
    subTotal: formatPrice(product.price * product.amount)
  })),
  total: formatPrice(
    state.cart.reduce((total, product) => {
      return total + product.price * product.amount
    }, 0)
  )
})

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(cartActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
