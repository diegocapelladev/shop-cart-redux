import { darken } from 'polished'
import styled from 'styled-components'

export const Wrapper = styled.main`
  background: #fff;
  border-radius: 4px;
  padding: 30px;
`

export const ProductTable = styled.table`
  width: 100%;

  thead th {
    color: #999;
    text-transform: uppercase;
    padding: 12px;
    text-align: left;
  }

  tbody td {
    padding: 12px;
    border-bottom: 1px solid #eee;
  }

  img {
    height: 100px;
  }

  strong {
    color: #333;
    display: block;
  }

  span {
    display: block;
    margin-top: 5px;
    font-size: 18px;
    font-weight: bold;
  }

  button {
    background: transparent;
    border: none;
    padding: 6px;
  }
`
export const QuantityBtn = styled.div`
  display: flex;
  align-items: center;

  input {
    border: 1px solid #ddd;
    border-radius: 4px;
    color: #666;
    padding: 6px;
    width: 60px;
    height: 30px;
    text-align: center;
    outline: none;

    &[type="number"] {
      -moz-appearance: textfield;
    }
  }

  > button {
    display: flex;
  }
`

export const Footer = styled.footer`
  margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  button {
    background: #7159c1;
    color: #fff;
    border: 0;
    border-radius: 4px;
    padding: 12px 20px;
    font-weight: bold;
    text-transform: uppercase;
    transition: background 0.2s;

    &:hover {
      background: ${darken(0.03, '#7159c1')};
    }
  }

  div {
    display: flex;
    align-items: baseline;

    span {
      color: #999;
      font-weight: bold;
      text-transform: uppercase;
    }

    strong {
      font-size: 28px;
      margin-left: 5px;
    }
  }
`
