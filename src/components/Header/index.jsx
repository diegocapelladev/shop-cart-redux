import { Link } from 'react-router-dom'
import { MdShoppingBasket } from 'react-icons/md'
import { connect } from 'react-redux'

import { Container } from '../Container'
import * as S from './styles'

import logo from '../../assets/logo.svg'

const Header = ({ cartSize }) => {
  return (
    <Container>
      <S.Wrapper>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>

        <S.Cart to="/cart">
          <div>
            <strong>Meu carrinho</strong>
            <span>{cartSize > '0' ? cartSize : '0'} itens</span>
          </div>
          <MdShoppingBasket size="36" color="#fff" />
        </S.Cart>
      </S.Wrapper>
    </Container>
  )
}

export default connect((state) => ({ cartSize: state.cart.length }))(Header)
