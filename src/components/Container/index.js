import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const Container = styled.div`
  ${({ theme }) => css`
    margin: 0 auto;
    width: 100%;
    max-width: ${theme.grid.container};

    ${media.lessThan('large')`
      padding: 0 calc(${theme.grid.gutter} * 2);
    `}

    ${media.lessThan('medium')`
      padding: 0 calc(${theme.grid.gutter} / 2);
    `}
  `}

`
